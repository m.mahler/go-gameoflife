package main

import (
	"fmt"
	"math/rand"
	"time"
)

const (
	ansiEscapeSeq = "\033c\x0c"
	brownSquare   = "\xF0\x9F\x9F\xAB"
	greenSquare   = "\xF0\x9F\x9F\xA9"
)

type World [][]*Ant

type Ant struct {
	Alive bool
}

type Game struct {
	prevGen    World
	currentGen World
	height     uint
	width      uint
}

func (g *Game) Init(width, height uint) {
	g.height, g.width = height, width
	emptyWorld := g.NewEmptyWorld()

	for x := range emptyWorld {
		for y := range emptyWorld[x] {
			if rand.Intn(4) == 1 {
				emptyWorld[x][y].Alive = true
			}
		}
	}

	g.currentGen = emptyWorld
}

func (g *Game) NewEmptyWorld() World {
	emptyWorld := make(World, g.height)
	for i := range emptyWorld {
		emptyWorld[i] = make([]*Ant, g.width)
	}

	for x := range emptyWorld {
		for y := range emptyWorld[x] {
			emptyWorld[x][y] = &Ant{}
		}
	}

	return emptyWorld
}

func (g *Game) NextGen() {
	prevWorld := g.currentGen
	nextWorld := g.NewEmptyWorld()

	for x := range nextWorld {
		for y := range nextWorld[x] {
			neighbors := getAliveNeighbors(prevWorld, x, y)
			if len(neighbors) == 2 || len(neighbors) == 3 {
				nextWorld[x][y].Alive = true
			}
		}
	}

	g.currentGen = nextWorld
}

func getAliveNeighbors(world World, x, y int) []*Ant {
	result := []*Ant{}
	coords := []struct {
		x int
		y int
	}{
		{x: x - 1, y: y},
		{x: x - 1, y: y + 1},
		{x: x - 1, y: y - 1},
		{x: x + 1, y: y},
		{x: x + 1, y: y + 1},
		{x: x + 1, y: y - 1},
		{x: x, y: y + 1},
		{x: x, y: y - 1},
	}

	for _, coord := range coords {
		if coord.x >= 0 && coord.y >= 0 && coord.x < len(world) && coord.y < len(world[0]) && world[coord.x][coord.y] != nil && world[coord.x][coord.y].Alive {
			result = append(result, world[coord.x][coord.y])
		}
	}

	return result
}

func (g *Game) Display() {
	for _, row := range g.currentGen {
		for _, cell := range row {
			switch {
			case cell.Alive:
				fmt.Printf(greenSquare)
			default:
				fmt.Printf(brownSquare)
			}
		}
		fmt.Printf("\n")
	}
}

func main() {
	game := &Game{}
	game.Init(20, 40)

	for {
		game.NextGen()
		game.Display()
		time.Sleep(100 * time.Millisecond)
		fmt.Println(ansiEscapeSeq)
	}
}
